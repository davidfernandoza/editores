<h1>Editores</h1>

Es la lista de paquetes y snippets que uso en diferentes editores

<h2>Visual studio code</h2>

<h3>Paquetes</h3>

* Material Icon Theme
*	<strong><em>Themes:</em></strong>
	* Tinacious Contrast Color Theme
	* One Dark Vivid (Kevin Kozee)
* laravel-blade
* GitLens
* EditorConfig for VS Code
* ESLint
* Live Server
* Python (Microsoft)
* vscode-language-todo
* Auto Close Tag
* Bracket Pair Colorizer
* change-case
* MySql Syntax
* Prettier - Code formatter
* React Native Tools

<h2>Sublime Text 3</h2>

<h3>Paquetes</h3>

* Seti_UI
* Theme - Seti Monokay
* Emmet
* Bootstrap 4 Autocomplete
* Laravel Blade Highlighter
* EditorConfig
* Auto​File​Name
* livereload
	* En el navegador hay que descargar la extencion de "livereload"
		* Se debe de habilitar en "Administrar extensiones" -> "Permitir acceso a URL de archivo"
		* Para correr el pluguin:
		1) se debe abrir el archivo en el navegador
		2) se debe activar (ejecutar) la extension del navegador
		3) se debe ejecutar el pluguin en Sublime

			a) ```Preferences/Package settings/LiveReload/Plugins/EnableDisable Plugins```

			 b) dar click a ->  ```LiveReload: Enable/disable plug-ins```

			 c) dar click a ->  ```Enable Simple Reload```
* jQuery


* Descargar Sublime 3 Merge: <a>https://www.sublimemerge.com/</a>


<h4>Diccionario (Spell Check)</h4>

* Descarga: <a>https://github.com/01luisrene/Diccionario-espanol-LibreOffice</a>

* Se descomprime y se copia el archivo <b>Language - Spanish.sublime-package</b>
* Se pega en ```C:\Program Files\Sublime Text 3\Packages```
* Se inicia en ```View + Dictionary + Lenguage - Spanish + es_ANY```
*

<h2>Atom</h2>

<h3>Paquetes</h3>

* atom-ide-ui
* atom-live-server
* auto-indend-file
* autocomplete-project-paths
* data-atom
* editorconfig
* emmet
* emmet-snippets-compatibility
	* Para que funcione con blade, sigue la ruta:
		``` Packages/emmet-snippets-compatibility```
		* View Code
			* keymaps/emmet-snippets-compatibility.cson

					'atom-text-editor[data-grammar="text html php blade"]:not([mini])':
						'tab': 'emmet:expand-abbreviation-with-tab'

					'atom-text-editor:not([mini])':
						'shift-space': 'emmet:expand-abbreviation'

					'atom-text-editor.autocomplete-active:not([mini])':
						'tab': 'autocomplete-plus:confirm'

					'.pane .editor:not(.mini)':
						'tab': 'snippets:expand'


* git-control
* goto-definition <strong><em>(navegacion de clases)</em></strong>
* highlight-selected
* ide-css
* ide-html
* ide-php
	* Para el lint:

		``` Packages/Ide Php```
		*  PHP path: <strong><em>(Windows)</em></strong>

			```C:\Xampp\php\php.exe```
* ide-python
	* Para el lint:

		``` Packages/Ide Python```
		*  Python Executable: <strong><em>(Windows)</em></strong>

			```C:\Python3\python3.exe```
* ide-typescript
* language-blade
* laravel
* linter-eslint
* minimap
* pigments
* platformio-ide-terminal (windows)
* Todo Show
* file-icons


<h2>Jetbrains</h2>
<h3>Temas</h3>

* one dark:  <a>https://github.com/yurtaev/idea-one-dark-theme</a>
